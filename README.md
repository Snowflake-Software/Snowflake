# ❄️ Snowflake

<div align="center"><img width="150rem" src="assets/logo.png" alt="The Snowflake Logo"></div align="center">
<!-- If anyone knows a better way to center an image, make a pull request. -->

---

## ⚠️ Warning

Multitasking doesn't fully work yet, because it doesn't send events to the processes. [(#1)](https://git.envs.net/Snowflake-Software/Snowflake/issues/1)

That means you can't really use Snowflake, since you can't run any processes and control them.

## ❓ What is it?

A modular system for ComputerCraft that extends CraftOS.

Note: If you want to contribute, [please see the contributing section](#contributing), it has important information about changing the `.gitignore` file.

## 💾 How to use/install

Copy all the files in this repository to the root of your ComputerCraft computer.

## 🤓 Nerdy stuff

### 📁 Filesystem hierarchy

- 🌳 `/` - The root directory.
    - 📂 `/boot/` -- Files related to the boot process.
        - 📄 `snowflake.lua` - The main file, starts the system.
        - 📄 `config.lua` - Configuration file.
        - 📂 `modules/` - The modules folder, contains modules like `multitasking.lua`.
    - 📂 `/libraries/` - Libraries.
    - 📂 `/programs/` - Programs, such as `sfconfig.lua`, a configuration tool for `/boot/config.lua`.
    - 📂 `/users/<username>/` - This is where user data is stored.

### 📦 Current modules

- `multitasking.lua` - A process manager.
- `crashKey.lua` - A module which crashes when the user presses a specific key.
- `example.lua` - An example module.

### ✏️ Contributing

When contributing, please use a global gitignore file that fits your system. You can read more about them [here](https://docs.github.com/en/get-started/getting-started-with-git/ignoring-files#configuring-ignored-files-for-all-repositories-on-your-computer).

If you try to add OS/Editor-specific files (such as Thumbs.db, or .vscode) to the gitignore, we will not merge your changes.

This does not mean you can't add stuff to `.gitignore`, just don't add OS/Editor-specific files.

Example:

```gitignore
# Good
users/
.cache/

# Bad
.vscode/
Thumbs.db
```

We use [Conventional Commits](https://conventionalcommits.org/) here. We also put a corresponding emoji according to [Gitmoji](https://gitmoji.dev) in the commit message.

Emojis are not required, but they are strongly encouraged.

```
Bad:
Fix Bugs

Good:
fix(multitasking): 🐛 pass events to processes
```

We also have a changelog system, please copy `changelog.py` to `.git/hooks/post-commit`.