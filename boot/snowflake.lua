-- This is the entrypoint of the system.
package.path = package.path .. ";/libraries/?.lua;/?.lua"

local exst = require("exst")

---@diagnostic disable-next-line: lowercase-global
snowflake = { loadedModules = {}, running = true }

local modules = {}

-- Adds a module to the system.
---@param module fun():nil @The function of the module to add.
---@param name string @The name of the module.
function snowflake.addModule(module, name)
    modules[name] = coroutine.create(module)
    table.insert(snowflake.loadedModules, name)
    snowflake.loadedModules[name] = true
end

local config = require("boot.config")

if not config then
    error("Not a valid config file.")
end



-- Logs a message to a file.
--
-- Levels:
-- 0: Debug
-- 1: Info
-- 2: Warning
-- 3: Error
-- 4: CRITICAL
--
---@param message string @The message to log.
---@param level number @The level/urgency of the message.
---@param type string @The subdirectory of the log file to write to.
function snowflake.log(message, level, type)
    local levelString = ""

    if level == 0 then
        levelString = "DEBG"
    elseif level == 1 then
        levelString = "INFO"
    elseif level == 2 then
        levelString = "WARN"
    elseif level == 3 then
        levelString = "ERRO"
    elseif level == 4 then
        levelString = "FAIL"
    end

    local dt = os.date("!*t") -- Gets the date table in UTC.
    -- Format using ISO 8601.
    local date = string.format("%04d-%02d-%02dT%02d:%02d:%02dZ", dt.year, dt.month, dt.day, dt.hour, dt.min, dt.sec)
    local filename = string.format("%04d-%02d-%02d.log", dt.year, dt.month, dt.day)

    if config.logLevel > level then
        return
    end

    if not fs.exists("/logs") then
        fs.makeDir("/logs")
    end

    for _,subdir in pairs(exst.split(type, "/")) do
        if not fs.exists("/logs/" .. subdir) then
            fs.makeDir("/logs/" .. subdir)
        end
    end

    local logHandle = fs.open("/logs/" .. type .. "/" .. filename, "a")
    local msg = "[" .. date ..", " .. levelString .. "]: " .. message
    logHandle.writeLine(msg)
    local debugger = peripheral.find("debugger")
    if debugger then
---@diagnostic disable-next-line: undefined-field
        debugger.print(type .. ": " .. msg)
    end
    logHandle.close()
end

-- Loads a module.
---@param name string @The name of the module to load.
function snowflake.loadModule(name)
    snowflake.log("Loading module " .. name .. "...", 1, "boot")
    local moduleHandle = fs.open("/boot/modules/" .. name .. ".lua", "r")
    local moduleContents = moduleHandle.readAll()
    if not moduleContents then
        snowflake.log("Module " .. name .. " is empty or doesn't exist.", 3, "boot")
        return
    end
    moduleHandle.close()
    if exst.startsWith(moduleContents, "-- depends-on: ") then
        local firstLine = exst.split(moduleContents, "\n")[1]
        local dependsOn = exst.split(string.sub(firstLine, #"--depends-on: "+1), ",")
        for _, depend in pairs(dependsOn) do
            if not snowflake.loadedModules[depend] then
                snowflake.log("Module " .. name .. " depends on module " .. depend .. " but it is not loaded, loading module " .. depend .. " now.", 1, "boot")
                snowflake.loadModule(depend)
            end
        end
    end
    local r,error = load(moduleContents, nil, nil, _ENV)

    if not r then
        snowflake.log("Module " .. name .. " failed to load, reason: " .. error, 4, "boot")
        return
    elseif type(r) == "function" then
        r()
    end
    snowflake.log("Module " .. name .. " loaded.", 1, "boot")
end

for _, module in pairs(config.enabledModules) do
    snowflake.loadModule(module)
end

local setEvent = false

parallel.waitForAny(function()
    while snowflake.running do
        coroutine.yield()
        for name,module in pairs(modules) do
            if coroutine.status(module) == "dead" then
                modules[name] = nil
            else
                local success, error =  coroutine.resume(module)
                if not success then
                    snowflake.log("Module " .. name .. " crashed, reason: " .. error, 4, "boot")
                end
            end
        end
        if setEvent then
            snowflake.currentEvent = {}
            setEvent = false
        end
    end
end, function ()
    while snowflake.running do
        local ev = {os.pullEvent()}
        if not setEvent then
            snowflake.currentEvent = ev
            setEvent = true
        end
        coroutine.yield()
    end
end)
