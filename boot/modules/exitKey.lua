snowflake.addModule(function() -- This will exit the system if the Numpad Add key is pressed, this is used for debugging.
    while snowflake.running do
        if snowflake.currentEvent and snowflake.currentEvent[1] == "key" then
            print(keys.getName(snowflake.currentEvent[2]))
            if snowflake.currentEvent[2] == keys.numPadAdd then
                snowflake.log("Exit key pressed, exiting.", 4, "modules/exitKey")
                snowflake.running = false
            end
        end
        coroutine.yield()
    end
end, "exitKey")