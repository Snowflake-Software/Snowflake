-- By default, this file is disabled because this is just an example.

snowflake.addModule(function() -- This will debug log the time every second.
    while snowflake.running do
        sleep(1)
---@diagnostic disable-next-line: param-type-mismatch
        snowflake.log(os.date(), 0, "modules/example")
        coroutine.yield()
    end
end, "example")

