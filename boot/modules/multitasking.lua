local allowedEvents = {
    ["key"] = true,
    ["char"] = true
}

snowflake.multitasking = { processes = {} }

-- Creates a new process.
---@param name string The name of the process.
---@param run string|function The path to the process or the function to run.
---@param customData table The custom data of the process.
---@return number @The PID of the newly created process.
function snowflake.multitasking.createProcess(name, run, customData)
    local customData = customData or {}
    local function cfunc()
    end

    if type(run) == "string" then
        cfunc = function()
            shell.run(run)
        end
    elseif type(run) == "function" then
        cfunc = run
    end

    local cr = coroutine.create(cfunc)

    local process = {
        name = name,
        coroutine = cr,
        customData = customData,
        pid = #snowflake.multitasking.processes + 1,
    }

    table.insert(snowflake.multitasking.processes, process)

    return #snowflake.multitasking.processes + 1 -- Return the PID of the newly created process.
end

snowflake.addModule(function()
    while snowflake.running do
        for i = 1, #snowflake.multitasking.processes do
            local process = snowflake.multitasking.processes[i]
            if coroutine.status(process.coroutine) == "dead" then
                table.remove(snowflake.multitasking.processes, i)
            else
                local success, error = coroutine.resume(process.coroutine)
                if not success then
                    snowflake.log("Process " .. process.name .. " exited, reason: " .. error, 0, "modules/multitasking")
                end
            end
        end
        coroutine.yield()
    end
end, "multitasking")
