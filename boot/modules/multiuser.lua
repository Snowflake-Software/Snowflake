--depends-on: multitasking

local exst = require("exst")
local exta = require("exta")
local json = require("json")

-- Check for users in the /config/users file.
-- The syntax is:
--[[
    username:pwdhash:groups:home:displayname:customdata 
    Password hash (pwdhash) is a SHA256 hash of the password.
    Groups, displayname, and customdata are optional.
    Home is the path to the user's home directory.
    Groups are specified as a comma-separated list of group names.
    If you want to specify a displayname, but not a group, you can do something like:
        bob:5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8::Bob:{"email":"bob@example.com"}
]]

local users = {}
local usersFile = fs.open("/config/users", "r")

-- Loop on each line in the file.
for line in usersFile.readLine do
    local rawUser = exst.split(line, ":")
    local user = {
        username = rawUser[1],
        pwdhash = rawUser[2],
        groups = exst.split(rawUser[3], ",") or {},
        home = rawUser[4] or ("/users/" .. rawUser[1]),
        displayname = rawUser[4] or rawUser[1],
    }

    if rawUser[5] then
        user.customdata = json.decode(rawUser[5])
    else
        user.customdata = {}
    end

    users[user.username] = user
end

snowflake.users = users

snowflake.debug("Loaded " .. #snowflake.users .. " users.")
for username, user in pairs(snowflake.users) do
    snowflake.debug("User " .. username .. ": " .. user.displayname .. " (" .. user.group[1] .. ")")
    snowflake.debug("Hash: " .. user.pwdhash)
end

-- Require all processes to have user and group.
local oldCreateProcess = snowflake.multitasking.createProcess
-- Creates a new process.
-- Custom data should have:
--[[
    createdBy: The PID of the process that the process was created by.
    user: The user that the process should run as.
]]
---@param name string The name of the process.
---@param run string|function The path to the process or the function to run.
---@param customData table The custom data of the process
---@return number @The PID of the newly created process.
function snowflake.multitasking.createProcess(name, run, customData)
    local customData = customData or {}
    if not customData.createdBy then
        -- Make it created by itself. (confusing sentence, I know)
        customData.createdBy = #snowflake.multitasking.processes + 1
    end
    local createdBy = snowflake.multitasking.processes[customData.createdBy]
    if not customData.user then
        customData.user = "nobody"
    end
    if 
        customData.user ~= "nobody"
        and customData.user ~= "root"
    then
        snowflake.log("Process " .. name .. " is not running as nobody or root.", 1, "modules/multiuser")
        if
            (createdBy.customData.user ~= customData.user)
            or (not exta.hasSome(
                snowflake.users[customData.user].groups,
                snowflake.users[createdBy.customData.user].groups
            ))
        then
            snowflake.log("User " .. createdBy.customData.user .. " is trying to impersonate " .. customData.user .. ".", 3, "modules/multiuser")
            error("User " .. createdBy.customData.user .. " is trying to impersonate " .. customData.user .. ".")
        end
    end

    if (not snowflake.users[customData.user]) or (not snowflake.users[customData.user] == "nobody") then
        snowflake.log("User " .. customData.user .. " does not exist.", 2, "modules/multiuser")
    end

    return oldCreateProcess(name, run, customData)
end

-- Not sure what else to do here.