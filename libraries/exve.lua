-- [Ex]tra [Ve]ctor
-- Utilities for working with vectors.

local exve = {}

-- Right now we don't need this, but it will come in handy later when we make windowing.

-- Checks if a point is inside a rectangle.
---@param x number @The x coordinate.
---@param y number @The y coordinate.
---@param tl table @The top-left point of the rectangle.
---@param br table @The bottom-right point of the rectangle.
---@return boolean @Whether the point is inside the rectangle.
function exve.inside(x, y, tl, br)
    return x >= tl.x and x <= br.x and y >= tl.y and y <= br.y
end

return exve