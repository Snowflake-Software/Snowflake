-- [Ex]tra [St]ring
-- Utilities for working with strings.

local exst = {}

-- Split a string into a table.
---@param s string The string to split.
---@param delimiter string The separator to split the string with.
---@return string[] The split string.
function exst.split(s, delimiter)
    local result = {}
    for match in (s..delimiter):gmatch("(.-)"..delimiter) do
        table.insert(result, match)
    end
    return result
end

-- Checks if a string starts with a certain string.
---@param s string The string to check.
---@param start string The string to check if s starts with.
---@return boolean Whether s starts with start.
function exst.startsWith(s, start)
    return string.sub(s, 1, #start+1) == start
end

-- Checks if a string ends with a certain string.
---@param s string The string to check.
---@param ending string The string to check if s ends with.
---@return boolean Whether s ends with ending.
function exst.endsWith(s, ending)
    return string.sub(s, -#ending) == ending
end

-- Trims a string of whitespaces.
---@param s string The string to trim.
---@return string The trimmed string.
function exst.trim(s)
    return s:match'^()%s*$' and '' or s:match'^%s*(.*%S)'
end

-- Checks if a string contains a certain string.
---@param s string The string to check.
---@param substr string The string to check if s contains.
---@return boolean Whether s contains contains.
function exst.contains(s, substr)
    return s:find(substr) ~= nil
end

return exst