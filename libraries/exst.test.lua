local exst = require("exst")

local function testSplit()
    local t = exst.split("a,b,c", ",")
    assert(#t == 3)
    assert(t[1] == "a")
    assert(t[2] == "b")
    assert(t[3] == "c")
    print("All tests passed!")
end

local function testStartsWith()
    assert(exst.startsWith("abc", "ab"))
    assert(not exst.startsWith("abc", "bc"))
    assert(not exst.startsWith("abc", "c"))
    assert(not exst.startsWith("abc", "abc"))
    print("All tests passed!")
end

local function testEndsWith()
    assert(exst.endsWith("abc", "bc"))
    assert(not exst.endsWith("abc", "ab"))
    assert(not exst.endsWith("abc", "a"))
    assert(not exst.endsWith("abc", "abc"))
    print("All tests passed!")
end

local function testTrim()
    assert(exst.trim("  abc  ") == "abc")
    assert(exst.trim("abc") == "abc")
    assert(exst.trim("") == "")
    print("All tests passed!")
end

local function testContains()
    assert(exst.contains("abc", "b"))
    assert(not exst.contains("abc", "d"))
    print("All tests passed!")
end

testSplit()
testStartsWith()
testEndsWith()
testTrim()
testContains()