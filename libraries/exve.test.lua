local exve = require("exve")

local function testInside()
    local tl = { x = 0, y = 0 }
    local br = { x = 10, y = 10 }
    for x = 0, 20 do
        for y = 0, 20 do
            write("x: " .. x .. ", y: " .. y .. ": ")
            if x > 10 or y > 10 then
                assert(not exve.inside(x, y, tl, br))
            else
                assert(exve.inside(x, y, tl, br))
            end
            print("Passed!")
        end
    end
    print("All tests passed!")
end

testInside()