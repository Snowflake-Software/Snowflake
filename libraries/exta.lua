-- [Ex]tra [Ta]ble
-- Utilities for working with tables.

local exta = {}

-- Checks if a table contains a certain value.
---@param t table The table to check.
---@param v any The value to check if t contains.
---@return boolean @Whether t contains v.
function exta.contains(t, v)
    for _, vv in ipairs(t) do
        if vv == v then
            return true
        end
    end
    return false
end

-- Joins a table into a string using a seperator.
---@param t table The table to join.
---@param sep string The seperator to use.
---@return string @The joined string.
function exta.join(t, sep)
    local s = ""
    for i, v in ipairs(t) do
        if i > 1 then
            s = s .. sep
        end
        s = s .. v
    end
    return s
end

-- Checks if a table has some of another table's values.
---@param t table The table to check.
---@param t2 table The table to check if t has some of its values.
---@return boolean @Whether t has some of t2's values.
function exta.hasSome(t, t2)
    for _, v in ipairs(t) do
        if exta.contains(t2, v) then
            return true
        end
    end
    return false
end

return exta