#!/bin/python3
import os
import subprocess

if __name__ == '__main__':
    print("Writing changelog...")
    changelogFile = open("CHANGELOG.md", "w")
    tags = subprocess.check_output(['git', 'tag', '-l']).decode('utf-8').split('\n\n')
    tags.reverse()
    tags = [x for x in tags if x]
    # Get the latest tag
    if len(tags) > 0:
        latest_tag = tags[0]
        # Get the commit messages inbetween the latest tag and the current HEAD
        commit_messages = subprocess.check_output(['git', 'log', '{}..HEAD'.format(latest_tag),
            '--format=- %B\n']
        ).decode('utf-8')
        commit_messages = commit_messages.split('\n\n\n')
        # Since commit messages can have multiple lines, we need to indent them
        # by 2 spaces so Markdown can render them properly.
        # We should not indent by 2 in the first line.
        commit_messages = [x.replace('\n', '\n  ') for x in commit_messages]
        commit_messages = '\n'.join(commit_messages)
    else:
        commit_messages = subprocess.check_output(['git', 'log',
            '--format=- %B\n']
        ).decode('utf-8')
        commit_messages = commit_messages.split('\n\n\n')
        commit_messages = [x.replace('\n', '\n  ') for x in commit_messages]
        commit_messages = '\n'.join(commit_messages)
    changelogFile.write('# HEAD\n\n')
    changelogFile.write(commit_messages)
    
    i = len(tags)
    ri = -1

    for tag in tags:
        i -= 1
        ri += 1
        changelogFile.write('\n# {}\n\n'.format(tag))
        if i == 0:
            commit_messages = subprocess.check_output(['git', 'log', '{}'.format(tag),
                '--format=- %B\n']
            ).decode('utf-8')
            commit_messages = commit_messages.split('\n\n\n')
            commit_messages = [x.replace('\n', '\n  ') for x in commit_messages]
            commit_messages = '\n'.join(commit_messages)
        else:
            commit_messages = subprocess.check_output(['git', 'log', '{}..{}'.format(tags[ri+1], tag),
                '--format=- %B\n']
            ).decode('utf-8')
            commit_messages = commit_messages.split('\n\n\n')
            commit_messages = [x.replace('\n', '\n  ') for x in commit_messages]
            commit_messages = '\n'.join(commit_messages)

                    
        changelogFile.write(commit_messages)
    
    changelogFile.close()

    _gs = subprocess.Popen(['git', 'status', '--porcelain'], stdout=subprocess.PIPE)
    recurse = subprocess.check_output(['grep', '-c', 'CHANGELOG.md'], stdin=_gs.stdout)

    if recurse != b'0':
        subprocess.run(['git', 'add', 'CHANGELOG.md'])
        subprocess.run(['git', 'commit', '--amend'], env={
            'VISUAL': '/bin/true',
        } | dict(os.environ))
        print("Committed changelog.")
    
    print("Done!")
